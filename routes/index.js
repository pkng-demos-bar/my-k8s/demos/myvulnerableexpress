
var express = require('express');
const crypto = require('crypto');

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

// write a post method that receives string input, execute a md5 hash, and return the output in hexstring  
router.post('/md5', function(req, res, next) {
  const { input } = req.body;
  
  if (!input || typeof input !== 'string') {
    return res.status(400).json({ error: 'Invalid input. Please provide a string.' });
  }

  const md5Hash = crypto.createHash('md5').update(input);
  
  res.json({ result: md5Hash.digest('hex') });
});

// Write a post method that uses pseudorandombytes to generate a random hex string of the specified length.
router.post('/random-hex', function(req, res, next) {
  const { length } = req.body;

  if (!length || typeof length !== 'number' || length <= 0) {
    return res.status(400).json({ error: 'Invalid length. Please provide a positive number.' });
  }

  const randomHex = crypto.pseudoRandomBytes(length).toString('hex');

  res.json({ result: randomHex });
});

router.post('/', function (req, res, next) {
  var title = req.body.title
  // ruleid:javascript-lang-pathtraversal-express-taint
  res.render('index', title)
})

module.exports = router;